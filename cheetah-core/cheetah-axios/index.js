/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import CheetahAxios from './cheetah-axios'
// const options = process.env.cheetahAxios || {}

// const config = {
//   baseURL: options.baseURL || '/',
//   isEncodeCodition: options.isEncodeCodition || false,
//   timeout: 300000
// }

const options = process.env.cheetahAxios
export default (context, inject) => {
  const cheetahAxios = new CheetahAxios(options, context)
  inject('cheetahAxios', cheetahAxios)
}

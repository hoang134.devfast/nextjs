import { LIST_CATEGORY } from '../../constants/listCategory'
const Constants = {

  methods: {
    convertCategoryName (id) {
      const categoryName = []
      const res = id.split(',')
      try {
        for (let index = 0; index < res.length; index++) {
          for (let idx = 0; idx < LIST_CATEGORY.length; idx++) {
            if (Number(res[index]) === LIST_CATEGORY[idx].id) {
              categoryName.push(LIST_CATEGORY[idx].name)
            }
          }
        }
        return categoryName.toString()
      } catch (error) {

      }
    }
  }
}

export default Constants

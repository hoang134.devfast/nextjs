const Liquor = {
  data () {
    return {
      treeData: [],
      treeOptions: {
        propertyNames: {
          text: 'MY_TEXT',
          children: 'KIDS',
          OPTIONS: false,
          state: { expanded: true }
        },
        dnd: true,
        checkbox: true,
        checkOnSelect: true
      }
    }
  },

  /**
     * Call API get users by condition search
     * Show data on table result
     *
     * @return {Object} API response for error handle
     */
  async fetch () {
    const res = await this.$cheetahAxios.getCategoryProductList(this.query)
    if (Array.isArray(res?.data)) {
      this.data = res.data
      this.total = res.meta?.total
    }
    if (this.data) {
      this.treeData = this.getTreeData()
    }
    return res
  },
  methods: {
    /**
     * Get list Category Product by Tree
     *
     * @param id
     * @returns Array
     */
    getTreeData (id) {
      if (!id) {
        id = 0
      }
      const chil = this.data.filter(el => el.parent_id === id)
      const result = []
      for (let index = 0; index < chil.length; index++) {
        result.push({
          MY_TEXT: chil[index].name,
          state: { expanded: true },
          OPTIONS: { expanded: true },
          id: chil[index].id,
          KIDS: this.getTreeData(chil[index].id)
        })
      }
      return result
    },

    parentCategory () {
      return this.listCategory.concat(this.options)
    }
  }
}

export default Liquor

/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const LocalStoreage = {
  methods: {
    /**
    * Set the value of the specified local storage item
    */
    setLocalStorage (keyname, value) {
      localStorage.setItem(keyname, value)
    },

    /**
     * Get the value of the specified local storage item
     */
    getLocalStorage (keyname) {
      return localStorage.getItem(keyname)
    },

    /**
     * Remove the the specified local storage item
     */
    removeLocalStorage (keyname) {
      localStorage.removeItem(keyname)
    },

    /**
     * Remove all local storage items
     */
    clearLocalStorage () {
      localStorage.clear()
    }
  }
}

export default LocalStoreage

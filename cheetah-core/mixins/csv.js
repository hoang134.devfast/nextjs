/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

import Encoding from 'encoding-japanese'
import Papa from 'papaparse'

export default {
  methods: {
    /**
     * Parse and encode csv data before export and download csv file
     */
    parseAndDowloadCsv (dataTarget) {
      const csv = Papa.unparse({
        data: dataTarget
      })
      const uniArray = Encoding.stringToCode(csv)
      const sjisArray = Encoding.convert(uniArray, {
        to: 'SJIS',
        from: 'UNICODE'
      })

      const unit8Array = new Uint8Array(sjisArray)
      const blob = new Blob([unit8Array], { type: 'text/csv;charset=Shift_JIS' })

      const link = document.createElement('a')
      // Browsers that support HTML5 download attribute
      const url = (window.URL || window.webkitURL).createObjectURL(blob)
      link.setAttribute('href', url)
      link.setAttribute('charset', 'Shift_JIS')
      link.setAttribute('download', +new Date() + '.csv')
      link.style.visibility = 'hidden'
      document.body.appendChild(link)
      link.click()
      setTimeout(() => { (window.URL || window.webkitURL).revokeObjectURL(link.href) }, 100)
      document.body.removeChild(link)
    }
  }
}

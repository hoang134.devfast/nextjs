/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const USER = 'user'

const UserLoginInfo = {
  methods: {
    /**
    * Set User login from local storage
    */
    setUserLogin (user) {
      this.setLocalStorage(USER, JSON.stringify(user))
    },

    /**
     * Get User login from local storage
     */
    getUserLogin () {
      return JSON.parse(this.getLocalStorage(USER))
    },

    /**
     * Remove User login from local storage
     */
    removeUserLogin () {
      this.removeLocalStorage(USER)
    }
  }
}

export default UserLoginInfo

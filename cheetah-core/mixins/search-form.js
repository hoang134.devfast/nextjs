/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const SearchFormHandler = {
  data () {
    return {
      resourceTypeName: null,
      loading: false
    }
  },

  computed: {
    /**
     * Get query
     */
    query () {
      const additionalParams = typeof this.additionalParams !== 'undefined' && Object.keys(this.additionalParams)
        ? this.additionalParams
        : {}

      return {
        ...this.paginationQuery,
        ...this.conditionQuery,
        ...additionalParams
      }
    }
  },

  methods: {
    /**
     * Refresh to get data
     */
    refresh () {
      this.doAsync(this.fetch)
        .catch((err) => {
          console.error(err)

          this.$toast.error(
            this.$t('messages.error.failed_to_get', { name: this.resourceTypeName })
          )
        })
    },

    /**
     * After the deletion confirmation dialog is confirmed
     */
    onDelete (id) {
      if (!id) { return }

      this.handleDelete(id)
        .then(() => {
          this.$toast.success(
            this.$t('messages.information.deleted', { name: this.resourceTypeName })
          )

          this.refresh()
        })
        .catch((err) => {
          console.error(err)

          this.$toast.error(
            this.$t('messages.error.failed_to_delete', { name: this.resourceTypeName })
          )
        })
    },

    /**
     * Search data
     * Set page = 1, limit = null
     *
     * @param {Number} val - Condition from search form
     */
    onSearch (condition) {
      let newCondition = {}

      if (
        typeof this.defaultCondition !== 'undefined' &&
        Object.keys(this.defaultCondition).length
      ) {
        newCondition = { ...newCondition, ...this.defaultCondition }
      }

      if (typeof condition !== 'undefined' && Object.keys(condition).length) {
        newCondition = { ...newCondition, ...condition }
      }

      this.setPageAndLimit(1, null)
      this.setCondition(newCondition)
      this.refresh()
    },

    /**
     * On page change
     *
     * @param {Number} page - Page number
     * @param {Number} limit - Page size number
     */
    onPageChange ({ page, limit }) {
      this.setPageAndLimit(page, limit)
      this.refresh()
    },

    /**
     * On page size change
     *
     * @param {Number} limit - Page size number
     */
    onPageSizeChange (limit) {
      this.setPageAndLimit(1, limit)
      this.refresh()
    }
  }
}

export default SearchFormHandler

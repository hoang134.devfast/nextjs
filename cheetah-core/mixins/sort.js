/*
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

const SortHandler = {
  data () {
    return {
      sortBy: '',
      sortType: 0
    }
  },

  computed: {
    /**
     * Get sort query
     *
     * @param {Object} Sort query
     */
    sortQuery () {
      if (!this.sortBy || !(this.sortType === 0 || this.sortType === 1)) {
        return {}
      }

      return {
        sortBy: this.sortBy,
        sortType: this.sortType
      }
    }
  },

  created () {
    this.getSortParamsFromRoute()
  },

  methods: {
    /**
     * Set sort id when url has sortId param
     */
    getSortParamsFromRoute () {
      this.setSort(this.$route.query.sortBy, +this.$route.query.sortType)
    },

    /**
     * Set sort
     *
     * @param {String} sortBy - Sort type
     * @param {Number} sortType - Sort type
     */
    setSort (sortBy, sortType) {
      if (!sortBy || !(sortType === 0 || sortType === 1)) {
        this.sortBy = ''
        this.sortType = 0
      } else {
        this.sortBy = sortBy
        this.sortType = sortType
      }

      this.setSortQueryToUrl()
    },

    /**
     * Set sort query to Url
     */
    setSortQueryToUrl () {
      let newRouteQuery = { ...this.$route.query }

      if (
        !this.sortQuery.sortBy ||
        !(this.sortQuery.sortType === 0 || this.sortQuery.sortType === 1)
      ) {
        delete newRouteQuery.sortBy
        delete newRouteQuery.sortType
      } else {
        newRouteQuery = {
          ...this.$route.query,
          sortBy: this.sortQuery.sortBy.toString(),
          sortType: this.sortQuery.sortType.toString()
        }
      }

      if (JSON.stringify(newRouteQuery) !== JSON.stringify(this.$route.query)) {
        this.$router.replace({ query: newRouteQuery }).catch((err) => {
          console.log(err)
        })
      }
    },

    /**
     * On sort
     *
     * @param {String} sortBy - Sort type
     * @param {Number} sortType - Sort type
     */
    onSort ({ sortBy, sortType }) {
      this.setSort(sortBy, sortType)
      this.refresh()
    }
  }
}

export default SortHandler

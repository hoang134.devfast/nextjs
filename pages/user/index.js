import Link from 'next/link'
import UserTable from '../../modules/user/components/UserTable'
import UserSearchForm from '../../modules/user/components/UserSearchForm'
import axios from 'axios'
import { useEffect, useState } from 'react'

export default function FirstPost() {
    const [users, setUsers] = useState([]);
    const [query, setQuery] = useState();
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        const data = axios.get('http://127.0.0.1:8000/api/users').then(res => {
            setUsers(res.data.data);
            setLoading(false);
        });
      }, []);

      const [message, setMessage] = useState('')

     const callbackFunction = (childData) => {
        setMessage(childData)
        console.log(message);
      }
    return ( 
        <> 
            <h1>Index</h1>
            {/* {data} */}
            <UserSearchForm/>
            <UserTable children= {users } parentCallback={callbackFunction} />
        </>
    ) 
    
  }
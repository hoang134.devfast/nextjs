import Head from 'next/head'

import Common from '../../../cheetah-core/mixins/common'
import axios from 'axios'
import Link  from 'next/dist/client/link'
export default function UserTable({ children }) {
    function editUser() {
        console.log(children)
    }
    function deleteUser () {
        console.log(Common);
        let id = this
        const data = axios.delete('http://127.0.0.1:8000/api/users/destroy/'+ id).then(res => {
        });
    }
    return (
    <> 
        <Head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"/>
        </Head>
       <h4>UserTable</h4>
        <table className="table table-dark">
        <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Email</th>
            <th scope="col">Name</th>
            <th scope="col">status</th>
            </tr>
        </thead>
        <tbody>
                {children.map(function(d, idx){
                    return (
                    <>
                    <tr>
                    <th key={idx}>{d.id}</th>
                    <td key={idx}>{d.email}</td>
                    <td key={idx}>{d.name}</td>
                    <td key={idx}>{d.status}</td>
                    <Link  href={`${d.id}`} >
                        <td><button>Edit</button></td>
                    </Link>
                    <td><button onClick={ deleteUser.bind(d.id)}>Delete</button></td>
                    </tr>
                    </>
                    )
                })}
        </tbody>
        </table>
    </>
    )
  }
/**
 * User status list
 */
export const USER_STATUS_LIST = [
  {
    name: 'user.statuses.inactive',
    value: 0
  },
  {
    name: 'user.statuses.active',
    value: 1
  }
]

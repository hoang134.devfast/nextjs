import { get } from 'lodash'

import Role from '../role/model'

export default class User {
  constructor (props) {
    this.id = get(props, 'id', 0)
    this.name = get(props, 'name', '')
    this.email = get(props, 'email', '')
    this.status = get(props, 'status', 0)
    this.password = get(props, 'password', null)
    this.roles = get(props, 'roles', []).map(item => new Role(item))

    // Action
    this.createAction = 'createUser'
    this.detailAction = 'getUser'
    this.updateAction = 'updateUser'
  }

  /**
   * Get form data
   */
  getFormData () {
    const newFormData = {
      name: this.name,
      email: this.email,
      status: this.status,
      roles: this.roles.map(item => item.id)
    }

    if (!this.id) {
      newFormData.password = this.password
    }

    if (this.id) {
      newFormData.id = this.id
    }

    return newFormData
  }
}

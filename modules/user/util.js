/**
 * Copyright 2021 DevFast Limited. All rights reserved.
 * Email: tech@devfast.us .
 */

/**
 * Get condition values
 */
export const getConditionValues = (params, userStatusList, roleList) => {
  if (
    !(Array.isArray(userStatusList) && userStatusList.length) ||
    !(Array.isArray(roleList) && roleList.length)
  ) {
    return {}
  }

  const newCondition = {}

  if (
    userStatusList.some(item => item.value === +params.status)
  ) {
    newCondition.status = +params.status
  }

  if (params.name) {
    newCondition.name = params.name
  }

  if (params.email) {
    newCondition.email = params.email
  }

  if (params.role) {
    const found = roleList.find(item => item.code === +params.role)

    if (found) {
      newCondition.role = found.code
      newCondition.currentRole = found
    }
  }

  return newCondition
}
